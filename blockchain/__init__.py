# -*- coding: utf-8 -*-
from flask import Flask

from .api import blockchain_api


def blockchain_node_factory():
    app = Flask(__name__)
    app.register_blueprint(blockchain_api)

    return app


__all__ = ['blockchain_node_factory']
