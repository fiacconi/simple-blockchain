# -*- coding: utf-8 -*-

TEMPLATE_ERROR_RESPONSE = '{{"message": "{0}", "error_type": "{1}", "error_message": "{2}"}}'

TEMPLATE_CHAIN_RESPONSE = '{{"chain": {0}, "length": {1}}}'

TEMPLATE_BLOCK_RESPONSE = '{{"block": {0}, "index": {1}}}'

TEMPLATE_TRANSACTIONS_RESPONSE = '{{"transactions": {0}, "length": {1}}}'

TEMPLATE_NEW_TRANSACTION = '{{"transaction": {0}, "blockIndex": {1}}}'

TEMPLATE_MINED_BLOCK = '{{"block": {0}, "index": {1}, "mining_sec": {2}, "mining_reward": {3}}}'


__all__ = [
    'TEMPLATE_ERROR_RESPONSE',
    'TEMPLATE_CHAIN_RESPONSE',
    'TEMPLATE_BLOCK_RESPONSE',
    'TEMPLATE_TRANSACTIONS_RESPONSE',
    'TEMPLATE_NEW_TRANSACTION',
    'TEMPLATE_MINED_BLOCK'
]
