# -*- coding: utf-8 -*-
import json
import os
from time import perf_counter as timer
import uuid

from flask import Blueprint, make_response, request
import requests

from .blockchain import blockchain_factory
from .utils import handle_resource_error
from .constants import TEMPLATE_CHAIN_RESPONSE, TEMPLATE_BLOCK_RESPONSE, \
    TEMPLATE_TRANSACTIONS_RESPONSE, TEMPLATE_NEW_TRANSACTION, TEMPLATE_MINED_BLOCK


# Block Chain instance stored in this blockchain
_BC = blockchain_factory(
    int(os.environ.get("BC_ORIGIN_PROOF", 100)),
    str(os.environ.get("BC_ORIGIN_HASH", "0")),
    int(os.environ.get("BC_DIFFICULTY", 4)),
    float(os.environ.get("BC_TRANSACTION_FEE_RATE", 0.01))
)


# UUID of the current node
_THIS_NODE_HOST = os.environ.get("BC_NODE_HOST", None)
_THIS_NODE_SIGNATURE = uuid.uuid4().hex

# List of other nodes in the network
_LIST_OTHER_NODES = os.environ.get("BC_NETWORK_NODES", [])
if _LIST_OTHER_NODES:
    _LIST_OTHER_NODES = _LIST_OTHER_NODES.split(',')

# Blueprint collecting block chain API
blockchain_api = Blueprint("blockchain_api", __name__, url_prefix='/api')


@blockchain_api.route("/chain", methods=['GET'])
@handle_resource_error("chain not found", 404)
def _get_full_chain():
    response = make_response(TEMPLATE_CHAIN_RESPONSE.format(str(_BC), len(_BC.chain)), 200)
    response.headers['Content-Type'] = 'application/json'
    return response


@blockchain_api.route("/chain/last", methods=['GET'])
@handle_resource_error("last block in chain not found", 404)
def _get_last_block():
    response = make_response(TEMPLATE_BLOCK_RESPONSE.format(str(_BC.last_block), len(_BC.chain) - 1), 200)
    response.headers['Content-Type'] = 'application/json'
    return response


@blockchain_api.route("/chain/<int:block_index>", methods=['GET'])
@handle_resource_error("block in chain not found, required index is not valid", 404)
def _get_chain_block(block_index):
    response = make_response(TEMPLATE_BLOCK_RESPONSE.format(str(_BC.chain[block_index]), block_index), 200)
    response.headers['Content-Type'] = 'application/json'
    return response


@blockchain_api.route("/transactions", methods=['GET'])
@handle_resource_error("transactions not found", 404)
def _get_pending_transactions():
    transaction_string = '[{}]'.format(','.join(str(transaction) for transaction in _BC.pending_transactions))
    response = make_response(
        TEMPLATE_TRANSACTIONS_RESPONSE.format(transaction_string, len(_BC.pending_transactions)), 200)
    response.headers['Content-Type'] = 'application/json'
    return response


@blockchain_api.route("/transactions/new", methods=['POST'])
@handle_resource_error("new transaction rejected", 404)
def _post_new_transaction():
    input_data = request.get_json()

    if not all(k in input_data for k in ["sender", "recipient", "amount"]):
        raise RuntimeError("Missing one of required fields: sender, receiver, or amount")

    block_index = _BC.insert_new_transaction(**input_data)

    response = make_response(TEMPLATE_NEW_TRANSACTION.format(str(_BC.pending_transactions[-1]), block_index), 201)
    response.headers['Content-Type'] = 'application/json'
    return response


@blockchain_api.route("/mine", methods=['GET'])
@handle_resource_error("could not mine a new block", 404)
def _mine_new_block():

    if len(_BC.pending_transactions) == 0:
        raise RuntimeError("no pending transactions to mine a new block")

    start = timer()
    proof_of_work = _BC.compute_new_proof()
    end = timer()

    if not _BC.assert_proof_is_valid(proof_of_work):
        raise RuntimeError("the computed PoW is not valid")

    for current_node in _LIST_OTHER_NODES:
        url = '{0}/api/chain/valid'.format(current_node)
        headers = {"Host": _THIS_NODE_HOST} if _THIS_NODE_HOST else None
        res = requests.get(url, headers=headers)
        if res.status_code != 200:
            raise RuntimeError("Chain on node {0} is not valid".format(current_node))

    for current_node in _LIST_OTHER_NODES:
        url = '{0}/api/chain/valid/{1}'.format(current_node, proof_of_work)
        headers = {"Host": _THIS_NODE_HOST} if _THIS_NODE_HOST else None
        res = requests.get(url, headers=headers)
        if res.status_code != 200:
            raise RuntimeError("PoW is not valid for node {0}".format(current_node))

    reward = _BC.compute_fee_on_pending_transactions(_THIS_NODE_SIGNATURE)
    mined_block = _BC.insert_new_local_block(proof_of_work)

    for current_node in _LIST_OTHER_NODES:
        url = '{}/api/chain/insert'.format(current_node)
        headers = {"Host": _THIS_NODE_HOST} if _THIS_NODE_HOST else None
        res = requests.post(url, json={"block": json.loads(str(mined_block))}, headers=headers)
        if res.status_code != 201:
            raise RuntimeError("Node {0} rejected the block - REMOTE ERROR: {1}".format(
                current_node, res.json()["error_message"]))

    response = make_response(TEMPLATE_MINED_BLOCK.format(str(mined_block), len(_BC.chain)-1, end-start, reward), 200)
    response.headers['Content-Type'] = 'application/json'
    return response


@blockchain_api.route("/chain/valid", methods=['GET'])
@handle_resource_error("consensus not found", 404)
def _check_chain_validity():
    if not _BC.assert_chain_is_valid():
        raise RuntimeError("chain on node {0} is not valid".format(_THIS_NODE_HOST))
    response = make_response('{"valid_chain": true}', 200)
    response.headers['Content-Type'] = 'application/json'
    return response


@blockchain_api.route("/chain/valid/<int:proof_of_work>", methods=['GET'])
@handle_resource_error("consensus not found", 404)
def _check_remote_pow(proof_of_work):
    if not _BC.assert_proof_is_valid(proof_of_work):
        raise RuntimeError("PoW is not valid for node {0}".format(_THIS_NODE_HOST))
    response = make_response('{{"proof_of_work": {0} "valid_pow": true}}'.format(proof_of_work), 200)
    response.headers['Content-Type'] = 'application/json'
    return response


@blockchain_api.route("/chain/insert", methods=['POST'])
@handle_resource_error("could not insert new block", 404)
def _insert_new_block():
    if request.headers["Host"] not in _LIST_OTHER_NODES:
        raise RuntimeError("Request from host {0} not allowed".format(request.headers["Host"]))

    input_data = request.get_json()
    last_block = _BC.insert_new_remote_block_from_json(input_data["block"])

    if not _BC.assert_chain_is_valid():
        _BC.chain.pop()
        raise RuntimeError("New chain is not valid")

    response = make_response('{{"block": {0}, "index": {1}}}'.format(str(last_block), len(_BC.chain) - 1), 201)
    response.headers['Content-Type'] = 'application/json'
    response.headers['Access-Control-Allow-Origin'] = \
        request.host_url if request.host_url in _LIST_OTHER_NODES else 'null'
    return response


__all__ = ['blockchain_api']
