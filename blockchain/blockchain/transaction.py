# -*- coding: utf-8 -*-
import json


class Transaction(object):

    def __init__(self, sender, recipient, amount):
        self.sender = sender
        self.recipient = recipient
        self.amount = amount

    def __str__(self):
        return json.dumps(self.__dict__, sort_keys=True)

    def __getitem__(self, item):
        return self.__dict__[item]


__all__ = ['Transaction']
