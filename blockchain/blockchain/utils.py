# -*- coding: utf-8 -*-


def safe_execution(function_name):

    def safe_function(todecorate):

        def safe_todecorate(*args, **kwargs):
            try:
                res = todecorate(*args, **kwargs)
            except Exception as e:
                raise RuntimeError("[ERROR - {0}] {1}".format(function_name, str(e)))
            else:
                return res

        return safe_todecorate

    return safe_function


__all__ = ["safe_execution"]
