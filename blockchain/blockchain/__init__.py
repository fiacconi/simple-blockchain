# -*- coding: utf-8 -*-
from .utils import safe_execution
from .chain import BlockChain


@safe_execution("blockchain.blockchain_factory")
def blockchain_factory(starting_proof=100, starting_hash="0", difficulty=4, transaction_fee_rate=0.01):
    return BlockChain(
        starting_proof=starting_proof,
        starting_hash=starting_hash,
        difficulty=difficulty,
        transaction_fee_rate=transaction_fee_rate
    )


__all__ = ['blockchain_factory']
