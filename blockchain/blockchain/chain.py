# -*- coding: utf-8 -*-
import hashlib

from .utils import safe_execution
from .block import Block
from .transaction import Transaction


class BlockChain(object):

    def __init__(self, starting_proof, starting_hash, difficulty, transaction_fee_rate):
        self.difficulty = difficulty
        self.transaction_fee_rate = min(1, max(0, transaction_fee_rate))
        self.pending_transactions = []
        self.chain = []
        self._set_up_origin_block(starting_proof, starting_hash)

    def __str__(self):
        return '[{0}]'.format(','.join(str(block) for block in self.chain))

    def __len__(self):
        return len(self.chain)

    def _set_up_origin_block(self, starting_proof, starting_hash):
        self.chain.append(Block([], starting_proof, starting_hash))
        self.last_block.timestamp = 0

    @property
    @safe_execution("BlockChain.last_block")
    def last_block(self):
        return self.chain[-1]

    @safe_execution("BlockChain.assert_proof_is_valid")
    def assert_proof_is_valid(self, proof, previous_block=None):
        if not previous_block:
            previous_block = self.last_block
        guess_hash = hashlib.sha256(
            '{0}{1}{2}'.format(proof, previous_block['proof'], previous_block.get_hash()).encode()
        ).hexdigest()
        return guess_hash[:self.difficulty] == ("0" * self.difficulty)

    @safe_execution('BlockChain.compute_new_proof')
    def compute_new_proof(self):
        proof = 0
        while not self.assert_proof_is_valid(proof):
            proof += 1
        return proof

    @safe_execution("BlockChain.insert_new_transaction")
    def insert_new_transaction(self, sender, recipient, amount):
        self.pending_transactions.append(Transaction(sender, recipient, amount))
        return len(self.chain)

    @safe_execution("BlockChain.insert_new_block")
    def insert_new_local_block(self, proof):
        self.chain.append(Block(self.pending_transactions, proof, self.last_block.get_hash()))
        self.pending_transactions.clear()
        return self.last_block

    @safe_execution("BlockChain.insert_new_block")
    def insert_new_remote_block_from_json(self, block_json):
        new_block = Block([], 0, "")
        new_block.timestamp = block_json["timestamp"]
        new_block.proof = block_json["proof"]
        new_block.previous_hash = block_json["previous_hash"]
        new_block.transactions = block_json["transactions"]
        self.chain.append(new_block)
        return self.last_block

    @safe_execution("BlockChain.compute_fee_on_pending_transactions")
    def compute_fee_on_pending_transactions(self, node_receiver):
        fee_transactions = [Transaction(t.sender, node_receiver, self.transaction_fee_rate * t.amount)
                            for t in self.pending_transactions]
        total_fee = sum(map(lambda t: t.amount, fee_transactions))
        self.pending_transactions += fee_transactions
        return total_fee

    @safe_execution("BlockChain.assert_chain_is_valid")
    def assert_chain_is_valid(self):
        if len(self.chain) == 1:
            return True
        for previous_block_index, current_block in enumerate(self.chain[1:]):
            if current_block.previous_hash != self.chain[previous_block_index].get_hash():
                return False
            if not self.assert_proof_is_valid(current_block.proof, self.chain[previous_block_index]):
                return False
        return True


__all__ = ['BlockChain']
