# -*- coding: utf-8 -*-
import datetime
import json
import hashlib
from typing import List


class Block(object):

    def __init__(self, transactions: List[object], proof: int, previous_hash: str):
        self.proof = proof
        self.previous_hash = previous_hash
        self.timestamp = datetime.datetime.utcnow().timestamp()
        self.transactions = [json.loads(str(transaction)) for transaction in transactions]

    def __str__(self):
        return json.dumps(self.__dict__, sort_keys=True)

    def __getitem__(self, item):
        return self.__dict__[item]

    def get_hash(self):
        return hashlib.sha256(self.__str__().encode()).hexdigest()


__all__ = ['Block']
