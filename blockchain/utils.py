# -*- coding: utf-8 -*-
from flask import make_response

from .constants import TEMPLATE_ERROR_RESPONSE


def handle_resource_error(msg, code):

    def resource_error_function(todecorate):

        def resource_error_todecorate(*args, **kwargs):
            res = None
            try:
                res = todecorate(*args, **kwargs)
            except Exception as e:
                res = make_response(TEMPLATE_ERROR_RESPONSE.format(msg, type(e).__name__, str(e)), code)
                res.headers['Content-Type'] = 'application/json'
            finally:
                return res

        resource_error_todecorate.__name__ = todecorate.__name__
        return resource_error_todecorate

    return resource_error_function


__all__ = ['handle_resource_error']
