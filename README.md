# Simple blockchain

## Introduction

This repository contains the Python implementation of a simple blockchain.
The main code is in the `blacokchain` package, which implements a blockchain node as a JSON REST API built as a `flask` application.
The web application is served through the `gunicorn` HTTP WSGI server.
The `blockchain.blockchain` sub-package contains the implementation of the blockchain logic, namely the storage of the chain, the Proof of Work computation algorithm and check, etc.

## Repository structure

The repository contains the following files.

```
simple-blockchain/
|__ .dockerignore
|__ .gitignore
|__ Dockerfile
|__ docker-compose.yaml
|__ README.md
|__ requirements.txt
|__ blockchain/
    |__ __init__.py
    |__ api.py
    |__ constants.py
    |__ utils.py
    |__ blockchain/
        |__ __init__.py
        |__ block.py
        |__ chain.py
        |__ transaction.py
        |__ utils.py
```

## How to run the application

### Docker Compose

The easiest way is via Docker Compose by running

```bash
docker-compose up
```

#### Building a blockchain network

The provided `docker-compose.yaml` file starts three services, namely `blockchain-node-1`, `blockchain-node-2`, and `blockchain-node-3` in order to mimic a blockchain network of 3 nodes.
When one node mines its own pending transaction, the new block is validated by the other nodes and then added to the corresponding chains.
The three nodes are available on `localhost` at the following ports:

- `blockchain-node-1` : `5000`

- `blockchain-node-2` : `5001`

- `blockchain-node-3` : `5002`

Note that the endpoint `/api/chain/insert` described below checks the hostname in the request and it verifies it is one among the hostnames *within* the Docker Compose network (i.e. not the bindings to the container host).

### Docker

Otherwise, one can use plain docker commands as follows.

```bash
...$ docker build -t blockchain-node .
...$ docker run --rm -p 5000:5000 blockchain-node
```

The container will be listening at port 5000 on the local host.

## API documentation

The application offers a JSON REST API with the following endpoints.

```
[GET : 200] /api/chain - returns the whole chain
                       - responds 404 in case of error

[GET : 200] /api/chain/last - returns the last block in the chain 
                            - responds 404 in case of error

[GET : 200] /api/chain/<id> - returns the block at position <id> 
                            - responds 404 in case of error

[GET : 200] /api/chain/valid - returns whether the chain is valid 
                             - responds 404 in case of error or when the chain is not valid

[GET : 200] /api/chain/valid/<proof_of_work> - returns whether the proof_of_work is valid 
                                             - responds 404 in case of error or when proof_of_work is not valid

[POST : 200] /api/chain/insert - add a new last block in the chain from posted JSON (accessible only within the blockchain network)
                               - responds 404 in case of error

[GET : 200] /api/mine - mines the chain to forge a new block
                      - responds 404 in case of error

[GET : 200] /api/transactions - returns the list of pending transactions
                              - responds 404 in case of error

[POST : 201] /api/transactions/new - adds a new transaction posting {"sender": string, "receiver": string, "amount": number}
                                   - responds 404 in case of error
```